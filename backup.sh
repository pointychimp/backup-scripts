#!/bin/bash

## configure this setting for sure ##
##
readonly FINAL_DESTINATION="/media/2TB-External/Daily-Backups/backup-$(date +'%Y.%m.%d').tar.gz" # this directory needs to exist already
##
#############################

# ${ARGS[0]}      file that contains a list of files/folders
#                     to backup. One file/folder per line.
readonly ARGS=($(echo $@ | tr " " "\n"))
readonly LIST_FILE="${ARGS[0]}"


readonly TMP_DIR="/tmp"
readonly WORKING_DIR="$TMP_DIR/backup-$(date +'%Y.%m.%d')" # this (and subdirs) will be cleaned up after the backup
readonly STAGE="$WORKING_DIR/files"
readonly LOG_FILE="$WORKING_DIR/copy.log"
readonly SOURCE_SUM_FILE="$WORKING_DIR/source.sums"
readonly DEST_SUM_FILE="$WORKING_DIR/destination.sums"
readonly PROBLEM_FILE_LOG="$WORKING_DIR/problemfiles.log"

readonly HASH_FUNC="rhash --md5 --recursive"
readonly COPY_FUNC="cp"
readonly COPY_ARGS="--archive --recursive --strip-trailing-slashes"

# make sure list file exists
[[ ! -f "$LIST_FILE" ]] && echo "Need a list file as first argument" && exit

# prepare some files and folders
mkdir -p "$STAGE"
touch "$LOG_FILE"
touch "$SOURCE_SUM_FILE"
touch "$DEST_SUM_FILE"
touch "$PROBLEM_FILE_LOG"

echo "Using $LIST_FILE as list file" >> "$LOG_FILE"

# act for each item in the list file
while read ITEM; do
	
	ITEM=$(eval echo "$ITEM") # to replace ~ with $HOME if needed

	[[ -z "$ITEM" ]] && continue # skip blank lines in $LIST_FILE

	$HASH_FUNC "$ITEM" >> "$SOURCE_SUM_FILE" # geneate hash(es) for everything on source side
	echo "Added hash(es) for $ITEM to $SOURCE_SUM_FILE" >> "$LOG_FILE" 
	$COPY_FUNC $COPY_ARGS "$ITEM" "$STAGE/" # actually copy over to the stage
	echo "Copied $ITEM to $STAGE/" >> "$LOG_FILE" 

done < "$LIST_FILE";

$HASH_FUNC "$STAGE/" >> "$DEST_SUM_FILE" # generate hashes for files on stage
echo "Added hashes for all backed up files to $DEST_SUM_FILE" >> "$LOG_FILE"

# go through the staged files' checksums and record
# any sums that are found on the destination side
# but not the source side
echo "------ Problem files in DESTINATION ------" >> "$PROBLEM_FILE_LOG"
while read A_DEST_LINE; do # get one line.                                  ex: "1cf683257cf8a92440ed421d1f71837b taxform.pdf"
	HASH=$(echo "$A_DEST_LINE" | awk '{print $1}') # extract just the hash. ex: "1cf683257cf8a92440ed421d1f71837b"
	WAS_FOUND=$(grep "^$HASH" "$SOURCE_SUM_FILE")  # when 'true', contains one line from $SOURCE_SUM_FILE, otherwise empty
	[[ -z "$WAS_FOUND" ]] && echo "$A_DEST_LINE" >> "$PROBLEM_FILE_LOG" # record the line from $DEST_SUM_FILE that was not found in $SOURCE_SUM_FILE
done < "$DEST_SUM_FILE"
# do the same thing, only in reverse
echo "------ Problem files in SOURCE ------" >> "$PROBLEM_FILE_LOG"
while read A_SOURCE_LINE; do
	HASH=$(echo "$A_SOURCE_LINE" | awk '{print $1}')
	WAS_FOUND=$(grep "^$HASH" "$DEST_SUM_FILE")
	[[ -z "$WAS_FOUND"  ]] && echo "$A_SOURCE_LINE" >> "$PROBLEM_FILE_LOG"
done < "$SOURCE_SUM_FILE"

# get rid of the check sum files as we don't want them to be backed up
rm "$SOURCE_SUM_FILE" "$DEST_SUM_FILE"
echo "Removed checksum files" >> "$LOG_FILE"
echo "No more logging. Hopefully the tar and clean up goes okay!" >> "$LOG_FILE"

# tar and compress everything into the final destination
tar --create --gzip --file "$FINAL_DESTINATION" -C "$TMP_DIR" $(echo "$WORKING_DIR" | sed "s|$TMP_DIR/||") # last bit is some magic to get /tmp/backup-date.he.re --> backup-date.he.re

# clean up
rm -rf "$WORKING_DIR"
