#!/bin/bash

# script is intended to be run once a day, though
# probably could easily be modified to update the tar
# instead or make differential backups or something.
DATE=$(date +'%Y.%m.%d')
LIST=$1 # first argument should be file containing a list of directories to backup
if [ ! -e $LIST ] 
then 
    echo "Error: File does not exist."
    exit 2
elif [[ -z "$LIST" ]]
then
    echo "Error: Need to specify list file."
    exit 2
fi
clear
DESTROOT="/media/2TB-External/Daily-Backups" # root of destination
CHECKSUMS="$DESTROOT/$DATE/sha1sums.txt"
ERRORLOG="$DESTROOT/$DATE/errors.txt"
echo -----------------------------------------
echo ---------- $(date +'%Y.%m.%d.%H.%M.%S') ----------
echo -----------------------------------------
cat $LIST | while read LINE ; do
    eval SOURCE=$LINE
    DESTINATION="$DESTROOT/$DATE/$SOURCE"
    echo "-------- $SOURCE --------"
    echo -n "Making dir ... "
    mkdir -p "$DESTINATION"
    echo "done!"
    echo -n "Copying ... "
    rsync -rctq  "$SOURCE/" "$DESTINATION"
    echo "done!"
    echo -n "Generating sums ... "
    rhash -Hr "$SOURCE" >> "$CHECKSUMS"
    echo "done!"
done
echo "---------------------------------"
# check sums and log any errors
echo -n "Checking sums ... "
rhash -Hrqc "$CHECKSUMS" "$DESTROOT/$DATE" | grep -v "OK" | grep -v "sha1sums.txt" | cut -f 3- -d\  > "$ERRORLOG"
rm "$CHECKSUMS"
echo "done!"
# delete error log if empty
if [ ! -s "$ERRORLOG" ]
then 
    rm "$ERRORLOG" 
else
    cat $ERRORLOG
    #echo "See error log"
fi
SIZE=$(du -s "$DESTROOT/$DATE" | cut -f 1)
# tar and compress everything up, showing progress while doing it
tar --directory "$DESTROOT/" -c $DATE --exclude-vcs --sparse | pv -i 0.25 -ptre -s ${SIZE}k | gzip -c > "$DESTROOT/$DATE.tar.gz" 
# remove non-compressed copy in $DESTINATION
echo -n "clean up ... "
rm -r "$DESTROOT/$DATE"
echo "done!"
exit 0
# eof
