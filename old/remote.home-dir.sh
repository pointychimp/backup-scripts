cd #!/bin/bash

# script is intended to be run once a day after
# the final local backup is done.
echo ---------------------------------------------
echo ---------- $(date +'%Y.%m.%d.%H.%M.%S') run ----------
echo ---------------------------------------------

DATE=$(date +'%Y.%m.%d')
SOURCE="/media/2TB-External/Daily-Backups"
DESTINATION="salsa@10.1.0.1:/home/salsa/Daily-Backups"
# options (in order):
# verbose, human-readable, compress on transfer, use ssh, show per-file progress
rsync -vhze ssh --progress "$SOURCE/$DATE.tar.gz" "$DESTINATION/"
